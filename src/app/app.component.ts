import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  public title = 'eng-angular-base';
  public showProducts = true;

  constructor(){
    setTimeout(() => {
      this.title = 'Angular Corso Base';
    }, 6000);
    //setInterval( () => this.showProducts = !this.showProducts, 2000);
}

  public changeTitle(): void{
    if(this.title === 'Angular Corso Base'){
      alert("Titolo Cambiato");
    }else{
      alert("Non cambiato");
    }
  }

}
