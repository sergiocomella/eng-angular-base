import { Directive, HostListener, ElementRef, Input } from '@angular/core';

@Directive({
  /**
   * Due tipologie di selettori:
   *  - Con le [] è un selettore attributo, non si potrà evocare tramite tag
   *    solo attraverso un attributo su un elemento già esistente.
   */
  selector: '[appHighlight]'
})
export class HighlightDirective {

  private _oldColor: string;
  
  @Input('appHighlight') 
  public colorSelected: string;

  constructor(private _elRef: ElementRef<HTMLElement>) {
    this._oldColor = _elRef.nativeElement.style.backgroundColor;
  }
  

  /**
   * HostListener(string)
   * Prende in input una striga che identifica l'evento
   */
  @HostListener('mouseover')
  public handleMouseOver(): void {
    this._elRef.nativeElement.style.backgroundColor = this.colorSelected;
  }

  @HostListener('mouseout')
  public handleMouseOut(): void{
    this._elRef.nativeElement.style.backgroundColor = this._oldColor;
  }
  

}
