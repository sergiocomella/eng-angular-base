import { Component, OnInit } from '@angular/core';
import { IToggleProduct } from 'src/app/interfaces/toggle-product.interface';
import { Product } from 'src/app/models/product';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss'],
})
export class ProductsListComponent implements OnInit {
  public cart: number = 0;
  public products: Product[];

  constructor() {
    this.products = [
      new Product('Oggetto Uno', 19.99),
      new Product('Oggetto Due', 20.5),
      new Product('Oggetto Tre', 59.99),
    ]; //new Product('Oggetto', 19.99);
  }

  ngOnInit(): void {}

  public handleToggleProduct(payload: IToggleProduct): void {
    if (payload.isAdd) {
      this.cart++;
    } else {
      this.cart--;
    }
  }

  public handleRemoveProduct(p: Product): void {
    this.products.splice(this.products.indexOf(p), 1);
  }
}
