import { Component, EventEmitter, OnInit, Output, Input, OnChanges, SimpleChange, SimpleChanges } from '@angular/core';
import { IToggleProduct } from 'src/app/interfaces/toggle-product.interface';
import { Product } from 'src/app/models/product';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit, OnChanges {

  @Input()
  public product: Product

  @Output() 
  public toggleProduct: EventEmitter<IToggleProduct> = new EventEmitter();

  @Output()
  public removeProduct: EventEmitter<Product> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(c: SimpleChanges): void{
    if('product' in c){
      console.log(c.product);
    }
  }

  public handleClick(isAdd: boolean): void{
    /**
     * Propago un evento all'esterno di questo componente
     */
    this.toggleProduct.emit({
      id: 0,
      isAdd
    });
  
  }

  public handleRemove(){
    this.removeProduct.emit(this.product)
  }

}
