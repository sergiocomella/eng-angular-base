import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { equalValidator } from 'src/app/validators/string.validators';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {


  public form: FormGroup;

  constructor() { 
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(8)]),
      confirmPassword: new FormControl('', [Validators.required, Validators.minLength(8)]),
      nome: new FormControl('', [Validators.required, Validators.minLength(3)]),
      cognome: new FormControl('', [Validators.required, Validators.minLength(3)]),
      eta: new FormControl('', [Validators.required]),
    }, [equalValidator('password', 'confirmPassword')], [])
  }

  ngOnInit(): void {
  }

  get email(): AbstractControl {
    return this.form.get('email')
  }

  get password(): AbstractControl{
    return this.form.get('password')
  }

  get confirmPassword(): AbstractControl{
    return this.form.get('confirmPassword')
  }

  get nome(): AbstractControl{
    return this.form.get('nome')
  }

  get cognome(): AbstractControl{
    return this.form.get('cognome')
  }

  get eta(): AbstractControl{
    return this.form.get('eta')
  }

  public handleSubmit(): void{
    if(this.form.invalid){
      return;
    }
    console.log('Success')
  }
}
