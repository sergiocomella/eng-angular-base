import { AbstractControl } from '@angular/forms'

/**
 * Funzione esterna
 * Accetta parametri utili ai fine dell'esecuzione della validazione
 */
export const rangeValidator = (min: number, max: number) => {
    //Funzione interna
    //Restituisce un valore solo in caso di errore
    return (c: AbstractControl) => {
        if(c.value < min || c.value > max){
            return {
                range: true
            }
        }
    }
}

