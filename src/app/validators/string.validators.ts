import { AbstractControl } from '@angular/forms'

export const equalValidator = (firstField: string, secondField: string) => {
    return (g: AbstractControl) => {
        if(g.get(firstField).value !== g.get(secondField).value){
            return {
                equal: true
            }
        }
    }
}